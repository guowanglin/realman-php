<?php



require "./vendor/autoload.php";

class Foo{

    private $bar;
    protected $a;
    public function __construct(Bar $bar, $a, $c)
    {
        $this->bar = $bar;
        $this->a = $a;
    }

    public function say()
    {
        echo  "hello $this->a";
    }
}


class Bar{

}



$container = new \src\Container();
$container->bind("cc", Foo::class, true);
$instance = $container->make("cc", ['a' => "我草你嘛", "c" => 'cccccc']);
$instance->say();

