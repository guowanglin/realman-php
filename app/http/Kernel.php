<?php

namespace app\http;

use app\middleware\BarMiddleware;
use app\middleware\FooMiddleware;
use \src\HttpKernel;

class Kernel extends HttpKernel
{
    protected $middlewares = [
        FooMiddleware::class,
        BarMiddleware::class
    ];
}