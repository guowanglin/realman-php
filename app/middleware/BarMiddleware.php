<?php
namespace app\middleware;

class BarMiddleware
{
    public function handle(\src\Request $request, \Closure $closure)
    {
        return $closure($request);
    }
}