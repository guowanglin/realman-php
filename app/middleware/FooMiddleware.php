<?php
namespace app\middleware;

class FooMiddleware
{
    public function handle(\src\Request $request, \Closure $closure)
    {
        return $closure($request);
    }
}