<?php
declare(strict_types=1);

namespace app\providers;

use src\Application;
use src\providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->routes(function (Application $app){
            require $app->basePath("routes/web.php");
        });
    }

    protected function map()
    {
    }
}