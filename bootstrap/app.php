<?php

$app = new \src\Application(dirname(__DIR__));

$app->singleton(\src\HttpKernel::class,
\app\http\Kernel::class
);
return $app;