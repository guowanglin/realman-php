<?php

return [
    "timezone" => "UTC",
    "providers" => [
        app\providers\RouteServiceProvider::class,
        \src\database\DatabaseServiceProvider::class
    ]
];