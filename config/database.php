<?php
return [
    'default' => 'mysql',
    'connections' => [
       'mysql' => [
           'read' => [
               [
                   'host' => '127.0.0.1',
                   'username' => 'root',
                   'password' => '123123',
                   'port' => 33061,
               ],
               [
                   'host' => '127.0.0.1',
                   'username' => 'root',
                   'password' => '123123',
                   'port' => 33062,
               ]
           ],
           'write' => [
               [
                   'host' => '127.0.0.1',
                   'username' => 'root',
                   'password' => '123123',
                   'port' => 33063,
               ],
               [
                   'host' => '127.0.0.1',
                   'username' => 'root',
                   'password' => '123123',
                   'port' => 33064,
               ]
           ],
           'driver' => 'mysql',
           'prefix' => '',
           'charset' => 'utf8mb4',
           'collation' => 'utf8mb4_unicode_ci',
           'host' => '',
           'port' => '',
           'database' => 'demo1',
           'username' => '',
           'password' => ''
       ]
    ]
];