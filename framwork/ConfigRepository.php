<?php
declare(strict_types=1);

namespace src;

use http\Exception\RuntimeException;
use src\interfaces\IConfigRepository;

class ConfigRepository implements IConfigRepository , \ArrayAccess
{
    protected $items = [];

    public function __construct($items = [])
    {
        $this->items = $items;
    }

    public function has($key)
    {
        return isset($this->items[$key]);
    }

    public function get($key, $default = null)
    {
        $array = $this->items;
        if (strpos($key, ".") !== false){
            foreach (explode(".", $key) as $segment){
                if (isset($array[$segment])){
                    $array = $array[$segment];
                }else{
                    return value($default);
                }
            }
        }
        return $array;
    }

    public function set($key, $value = null)
    {
        $this->items[$key] = $value;
    }

    public function all()
    {
        return $this->items;
    }

    public function offsetExists($offset)
    {
        // TODO: Implement offsetExists() method.
    }

    public function offsetGet($offset)
    {
        return $this->get($offset, null);
    }

    public function offsetSet($offset, $value)
    {
        // TODO: Implement offsetSet() method.
    }

    public function offsetUnset($offset)
    {
        // TODO: Implement offsetUnset() method.
    }
}