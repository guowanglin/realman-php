<?php
declare(strict_types=1);

namespace src;

use src\interfaces\IResponse;

class Response implements IResponse
{
    protected $statusCode;
    protected $content;
    protected $headers;
    protected $version = "1.1";
    protected $statusText;

    /**
     * @param $code
     * @param $content
     * @param $type
     */
    public function __construct($content = '', $statusCode = 200, array $headers = [])
    {
        $this->content = $content;
        $this->statusCode = $statusCode;
        $this->headers = $headers;
    }

    public function send()
    {
        $this->sendHeaders();
        $this->sendContent();
        if (\function_exists('fastcgi_finish_request')) {
            fastcgi_finish_request();
        } elseif (\function_exists('litespeed_finish_request')) {
            litespeed_finish_request();
        } elseif (!\in_array(\PHP_SAPI, ['cli', 'phpdbg'], true)) {
            flush();
        }
        return $this;
    }

    protected function sendHeaders()
    {
//        header(sprintf('HTTP/%s %s %s', $this->version, $this->statusCode, $this->statusText));
    }

    protected function sendContent()
    {
        echo $this->content;
        return $this;
    }
}