<?php

namespace src\bootstrap;

use src\Application;

/*
 * 引导启动服务提供者
 */
class BootProviders
{
    public function bootstrap(Application $application)
    {
        return $application->boot();
    }
}