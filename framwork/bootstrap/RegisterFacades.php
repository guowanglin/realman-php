<?php

namespace src\bootstrap;

use src\Application;
use src\Facade;

class RegisterFacades
{
    public function bootstrap(Application $app)
    {
        Facade::setFacadeApplication($app);
    }
}