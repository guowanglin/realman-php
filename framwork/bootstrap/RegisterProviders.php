<?php
declare(strict_types=1);

namespace src\bootstrap;

use src\Application;

class RegisterProviders
{
    public function bootstrap(Application $app)
    {
        $app->registerConfiguredProviders();
    }
}