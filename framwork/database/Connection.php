<?php
declare(strict_types=1);

namespace src\database;

class Connection
{
    /**
     * @var \PDO|\Closure $pdo
     */
    protected $pdo;

    protected $database;

    protected $tablePrefix;

    protected $config = [];

    public function __construct($pdo, $database = '', $tablePrefix = '', array $config = [])
    {
        $this->pdo = $pdo;
        $this->database = $database;
        $this->tablePrefix = $tablePrefix;
        $this->config = $config;
    }
}