<?php
declare(strict_types=1);

namespace src\database;

interface ConnectionInterface
{
    public function table($table, $as = null);
    public function raw($value);
    public function selectOne($query, $bindings = [], $useReadPdo = true);
    public function select($query, $bindings = [], $useReadPdo = true);
    public function insert($query, $bindings = []);
    public function update($query, $bindings = []);
    public function delete($query, $bindings = []);
    public function statement($query, $bindings = []);
    public function beginTransaction();
    public function commit();
    public function rollBack();
}