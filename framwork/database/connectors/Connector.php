<?php
declare(strict_types=1);

namespace src\database\connectors;

use PDO;
use src\interfaces\IConnectorInterface;

class Connector
{
    protected $options = [
        PDO::ATTR_CASE => PDO::CASE_NATURAL,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_ORACLE_NULLS => PDO::NULL_NATURAL,
        PDO::ATTR_STRINGIFY_FETCHES => false,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];


    /**
     * 创建一个pdo连接
     * @param $dsn
     * @param array $config
     * @param array|null $options
     * @return PDO
     */
    public function createConnection($dsn, array $config, array $options)
    {
        [$username, $password] = [
                $config['username'] ?? null, $config['password'] ?? null,
        ];
        return new PDO($dsn, $username, $password, $options);
    }
}