<?php
declare(strict_types=1);

namespace src\facades;

use src\Facade;

class Db extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'db';
    }
}