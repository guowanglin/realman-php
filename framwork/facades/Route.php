<?php
declare(strict_types=1);
namespace src\facades;

use src\Facade;

/**
 *
 * @method static \src\Route post(string $uri, array|string|callable|null $action = null)
 * @method static \src\Route get(string $uri, array|string|callable|null $action = null)
 */
class Route extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'router';
    }
}