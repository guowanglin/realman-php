<?php
declare(strict_types=1);

namespace src\interfaces;

interface IConfigRepository
{
    public function has($key);
    public function get($key, $default);
    public function set($key, $value = null);
    public function all();
}