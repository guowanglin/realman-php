<?php

namespace src\interfaces;

interface IConnectorInterface
{
    public function connect(array $config);
}