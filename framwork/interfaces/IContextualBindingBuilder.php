<?php

namespace src\interfaces;

interface IContextualBindingBuilder
{
    public function needs($abstract);

    public function give($implementation);
}