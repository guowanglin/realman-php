<?php

namespace src\interfaces;

interface IRequest
{
    public function getMethod();
    public function getUri();
    public function post($key = null);
    public function get($key = null);
    public function header($key = null);
    public function file($name);
}