<?php

namespace src\interfaces;

interface IResponse
{
    public function send();
}