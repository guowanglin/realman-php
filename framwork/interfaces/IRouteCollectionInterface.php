<?php

namespace src\interfaces;

use src\Request;
use src\Route;

interface IRouteCollectionInterface
{
    public function add(Route $route);
    public function match(Request $request);
    public function get($method = null);
    public function hasNamedRoute($name);
    public function getByName($name);
    public function getByAction($action);
    public function getRoutes();
}