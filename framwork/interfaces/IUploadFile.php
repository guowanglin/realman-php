<?php

namespace src\interfaces;

interface IUploadFile
{
    public function getStream();
    public function moveTo($path, $newName);
    public function getSize();
    public function getFileType();
}