<?php
declare(strict_types=1);
namespace src\providers;

use Closure;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace;
    protected $loadRoutesUsing;

    public function register()
    {
        $this->booted(function (){
            //在服务容器booted加载路由
            $this->loadRoutes();
        });
    }

    protected function routes(Closure $routesCallback)
    {
         $this->loadRoutesUsing = $routesCallback;
        return $this;
    }


    protected function loadRoutes()
    {
        ($this->loadRoutesUsing)($this->app);
    }
}