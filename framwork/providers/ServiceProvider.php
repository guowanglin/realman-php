<?php
declare(strict_types=1);

namespace src\providers;

use Closure;
use src\Application;

abstract class ServiceProvider
{
    protected ?Application $app;
    protected $bootedCallbacks =[];
    protected $bootingCallbacks =[];

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function register(){}
    public function boot(){}

    public function booted(Closure $callback)
    {
        $this->bootedCallbacks[] = $callback;
    }

    public function booting(Closure $callback)
    {
        $this->bootingCallbacks[] = $callback;
    }

    //服务提供者booting
    public function callBootingCallbacks()
    {
        $index = 0;
        while ($index < count($this->bootingCallbacks)) {
            call_user_func($this->bootingCallbacks[$index]);
            $index++;
        }
    }

    //服务提供者booted
    public function callBootedCallbacks()
    {
        $index = 0;
        while ($index < count($this->bootedCallbacks)) {
            call_user_func($this->bootedCallbacks[$index]);
            $index++;
        }
    }

}