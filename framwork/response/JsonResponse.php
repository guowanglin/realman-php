<?php

namespace src\response;

use src\interfaces\IResponse;
use src\Response;

class JsonResponse extends Response
{
    public function __construct($content = '', $status = 200, array $headers = [])
    {
        parent::__construct($content, $status, $headers);
        $this->headers['Content-Type'] = 'application/json';
    }

    public function send()
    {

    }
}