<?php
use src\HttpKernel;

require dirname(__DIR__)."/vendor/autoload.php";

$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

$app = require_once __DIR__.'/../bootstrap/app.php';

$kernel = $app->make(HttpKernel::class);

$response = $kernel->handle(
    $request = new \src\Request()
)->send();
